//make connection
let socket = io.connect('http://localhost:3000');

//query Dom
let message = document.getElementById('message'),
    handle = document.getElementById('handle'),
    btn = document.getElementById('send'),
    output = document.getElementById('output'),
    feedback = document.getElementById('feedback');

//emit events

btn.addEventListener('click', () => {
    //when a button is clicked then socket will emit the values to server..
    socket.emit('chat', {
        message: message.value,
        handle: handle.value
    });
});

//attach event listener ...keypress

message.addEventListener('keypress', () => {

    socket.emit('typing', handle.value);
});

//listen for events
socket.on('chat', (data) => {
    feedback.innerHTML = '';
    output.innerHTML += "<p><strong>" + data.handle + ":</strong>" + data.message + "</p>"
});

socket.on('typing', (data) => {
    feedback.innerHTML = '<p><em>' + data + 'is typing a message..</em></p>'
});