let express = require('express');
let socket = require('socket.io');
let app = express();
//server setup
let server = app.listen(3000, () => {
    console.log('app is listening to port 3000');
});

//set static middleware..
app.use(express.static('public'));

//socket setup
let io = socket(server);

io.on('connection', (socket) => {
    console.log('made socket connection');
    //when socket gets chat message that is comming from chat.js then it will emit all data to clients
    socket.on('chat', (data) => {
        //emiting the data(message and handle to other clients in the browser)
        io.sockets.emit('chat', data);
    });

    socket.on('typing', (data) => {
        socket.broadcast.emit('typing', data)
    });
});